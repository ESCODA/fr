---
title: Exercice avec l'afficheur LCD 16x2 Grove en C/C++ avec Stm32duino
description: Exercice avec l'afficheur LCD 16x2 Grove en C/C++ avec Stm32duino
---
# Exercice avec l'afficheur LCD 16x2 Grove en C/C++ avec Stm32duino

<div align="center">
<img alt="Grove - LCD 16x2 Display" src="images/grove-lcd_16x2_display.jpg" width="600px">
</div>

Bientôt disponible

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
