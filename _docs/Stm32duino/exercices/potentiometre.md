---
title: Exercice avec le potentiomètre Grove en C/C++ pour Stm32duino
description: Exercice avec le potentiomètre Grove en C/C++ pour Stm32duino
---

# Exercice avec le potentiomètre Grove en C/C++ pour Stm32duino

- **Prérequis :**

Pour ce capteur il faudra le brancher sur le pin A0.
Ouvrez Arduino et vérifiez que le port est connecté: Outils/Port, COM3 devrait être sélectionné.

- **Le potentiomètre :**

![Image](images/7_potentiometre/potentiometre.png)

Le potentiomètre, *rotary angle sensor* en anglais, renvoi une valeur comprise entre 0 et 1024. Il permet de moduler manuellement des valeurs à utiliser (par exemple pour faire varier l'instensité sonore d'un buzzer *cf exemple de tp*).

*Voici le code sur Arduino*

```c
void setup() {
  Serial.begin(9600);
  pinMode(A0,INPUT);

}

void loop() {

  Serial.println(analogRead(A0));
  delay(500);

}
```
On affiche toute le 500ms la valeur lue au pin A0.

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
