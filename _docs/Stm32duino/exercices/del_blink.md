---
title: Exercice avec la DEL utilisateur en C/C++ pour Stm32duino
description: Exercice avec la DEL utilisateur en C/C++ pour Stm32duino
---

# Exercice avec la DEL utilisateur en C/C++ pour Stm32duino

Dans cet exemple, nous voulons faire clignoter une DEL utilisateur de votre STM32.

Une fois Arduino IDE configuré pour votre STM32.

Ouvrez le croquis `Blink` dans `Fichier > Exemples > 01.Basics > Blink`

![Image](images/2_led_clignote/1.png)

Le programme permettant de faire clignoter une LED sur votre STM32 s'ouvre alors.

![Image](images/2_led_clignote/2.png)

Cliquez sur le bouton `Téléverser` (représenté par une flèche allant de gauche a droite)

![Image](images/2_led_clignote/3.png)

Une LED sur votre STM32 devrait clignoter, dans notre exemple (STM32 F446RE) il s'agit de `LED2`, une DEL de couleur verte.
