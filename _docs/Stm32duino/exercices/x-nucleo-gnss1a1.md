---
title: Exercice avec la carte de géolocalisation GNSS X-NUCLEO-GNSS1A1 en C/C++ pour Stm32duino
description: Exercice avec la carte de géolocalisation GNSS X-NUCLEO-GNSS1A1 en C/C++ pour Stm32duino
---
# Exercice avec la carte de géolocalisation GNSS X-NUCLEO-GNSS1A1 en C/C++ pour Stm32duino

Vous devez disposez de la [carte de géolocalisation GNSS X-NUCLEO-GNSS1A1](https://www.st.com/en/ecosystems/x-nucleo-gnss1a1.html) pour continuer cet exercice.

## Démarrage

La carte X-NUCLEO-GNSS1A1 est une de géolocalisation GNSS muni d'un module GNSS . Elle est munie d'un module Teseo-LIV3F de ST Microelectronics. 

![image](images/x-nucleo-gnss1a1.jpg)

## Branchement

Brancher la carte X-NUCLEO-GNSS1A1, attention à bien respecter le marquage des connecteur : CN9 -> CN9, CN5 -> CN5, etc...

## Installation des bibliothèques pour la carte d'extension IKS01A3

### Sur Windows
A faire

### Sur Linux
A faire

### Sur MacOS
Entrez les commandes suivantes :
```bash
wget https://github.com/stevemarple/MicroNMEA/zip/master -O MicroNMEA-master.zip
unzip MicroNMEA-master.zip
wget https://github.com/stm32duino/X-NUCLEO-GNSS1A1/zip/master -O X-NUCLEO-GNSS1A1-master.zip
unzip X-NUCLEO-GNSS1A1-master.zip
```

## Utilisation via l'UART

Lancez l'IDE Arduino (préalablement configuré pour Stm32duino).

> Relancer l'IDE Arduino si les exemples de la bibliothèque n'apparait pas dans le menu `Fichier > Exemples`.

Ouvrez le croquis d'exemple `Fichier > Exemples > STM32Duino X-NUCLEO-GNSS1A1 > X_NUCLEO_GNSS1A1_MicroNMEA_UART`.

Ouvrez la console série qui affiche les traces suivantes:
```

```

## Utilisation par l'I2C

Lancez l'IDE Arduino (préalablement configuré pour Stm32duino).

> Relancer l'IDE Arduino si les exemples de la bibliothèque n'apparait pas dans le menu `Fichier > Exemples`.

Ouvrez le croquis d'exemple `Fichier > Exemples > STM32Duino X-NUCLEO-GNSS1A1 > X_NUCLEO_GNSS1A1_MicroNMEA_I2C`.

Ouvrez la console série qui affiche les traces suivantes:
```

```

## Documentation

* https://github.com/stm32duino/X-NUCLEO-GNSS1A1

* TESEO-LIV3F datasheet https://www.st.com/content/st_com/en/products/positioning/gnss-modules/teseo-liv3f.html
