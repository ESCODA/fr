---
title: Exercices avec RIOT OS pour la carte P-NUCLEO-WB55
description: Exercices avec RIOT OS pour la carte P-NUCLEO-WB55

---
# Exercices avec RIOT OS pour la carte P-NUCLEO-WB55

Les exercices avec RIOT OS pour la carte P-NUCLEO-WB55 arrivent très vite !

En attendant, vous pouvez suivre les exercices du [cours RIOT](https://github.com/RIOT-OS/riot-course/blob/master/README.md) en utilisant une autre carte Nucleo.

