---
title: Tutoriel Afficheurs 7 segments
description: Tutoriel basé sur un afficheur 7 segments et un driver TM1637
---
# Tutoriel Afficheur 7-segments

<div align="center">
<img alt="Grove - 4-Digit Display" src="images/grove-4_digit_display.jpg" width="400px">
</div>

Pour utiliser l'afficheur, vous aurez besoin des bibliothèque **pyb** et **tm1637**.
pyb est déjà présente, il vous faut installer tm1637. Déplacez le fichier **tm1637.py** dans le dossier où se trouve **main.py**.
La bibliothèque est installée ! Vous pouvez à présent utiliser ces deux bibliothèques avec :
```
from pyb import Pin
import tm1637
```
A présent, vous allez définir votre accès à l'afficheur sur A0 avec l'instruction suivante :

```
tm = tm1637.TM1637(clk=Pin('A0'), dio=Pin('A1'))
```

Vous pouvez maintenant modifier l'affichage en utilisant :
```
tm.write([<case1>, <case2>, <case3>, <case4>])
```
Il est possible de contrôler l’affichage des 7 segments indépendamment avec un mot binaire de la forme 0bABCDEFG.
Mettez simplement un 1 ou un 0 selon que vous voulez allumer ou éteindre le segment.  
Par exemple, pour afficher 0123, écrivez :  
```
tm.write([0b0111111, 0b0000110, 0b1011011, 0b1001111])
```

![Afficheur 7 segments Label](images/300px-7_segment_display_labeled.jpg)



> Crédit image : [Wikipedia](https://fr.wikipedia.org/wiki/Fichier:7_segment_display_labeled.svg), [Seedstudio](http://wiki.seeedstudio.com/Grove-4-Digit_Display/)
