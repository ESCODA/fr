---
title: Exercice avec l'adaptateur Nintendo Nunchuk Grove en MicroPython
description: Exercice avec l'adaptateur Nintendo Nunchuk Grove en MicroPython
---

# Exercice avec l'adaptateur Nintendo Nunchuk Grove en MicroPython

<div align="center">
<img alt="Grove - Nintendo Nunchuk Adapter" src="images/grove-nunchuk.jpg" width="400px">
</div>

Bientôt disponible


> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
