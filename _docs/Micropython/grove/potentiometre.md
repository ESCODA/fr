---
title: Tutoriel Potentiomètre
---

# Tutoriel d'utilisation du potentiomètre en MicroPython avec une carte STM32WB55

- **Prérequis :**

Le kit de capteurs fournit un *shield* (image ci-dessous) qui s’adapte sur la carte et simplifie la connexion des capteurs.
Les broches Dx permettent de traiter un signal digital (0 ou 1) or les broches Ax gèrent les signaux analogiques.

![Image](images/shield.png)
![Image](images/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](images/pybflash.png)

Le fichier main.py sera exécuté par défaut au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).

Pour ce capteur il faudra le brancher sur la **broche A1**.

- **Le potentiomètre :**

![Image](images/potentiometre.png)

Le potentiomètre, *rotary angle sensor* en anglais, permet de moduler manuellement une tension d’entrée comprise entre 0V et +3.3V (pour notre carte). Après conversion de cette tension (par l’ADC de la broche analogique sur laquelle est connecté le potentiomètre) le microcontrôleur reçoit une valeur entière comprise entre 0 et 4095. Cette valeur peut ensuite servir à piloter d’autres actuateurs, par exemple à faire varier l’intensité sonore d’un buzzer.


Code MicroPython :
```python
import pyb
from pyb import Pin, ADC
import time

adc = ADC(Pin('A1'))
while(True):
	print(adc.read()) # read value, 0-4095
	time.sleep(1)      # sleep for 500 milliseconds

````
