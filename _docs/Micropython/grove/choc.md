---
title: Tutoriel Tilt Sensor
---

# Tutoriel d'utilisation du capteur tilt-sensor en MicroPython avec une carte STM32WB55

- **Prérequis :**

Le kit de capteurs fournit un *shield* (image ci-dessous) qui s’adapte sur la carte et simplifie la connexion des capteurs.
Il suffit de le connecter à la carte.
Les broches Dx permettent de traiter un signal digital (0 ou 1) or les broches Ax gèrent les signaux analogiques.

![Image](images/shield.png)
![Image](images/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](images/pybflash.png)

Le fichier main.py sera exécuté par défaut au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).

Pour ce capteur il faudra le brancher sur la **broche D4**.

- **Capteur Tilt-sensor :**

![Image](images/tiltsensorim.png)

•	Le tilt-sensor, capteur d’inclinaison en français, est un capteur change d’état lorsque l’inclinaison par rapport à la verticale dépasse une valeur limite donnée (la bille dans le capteur suit son mouvement, roule et vient faire contact).
Ce capteur peut donc être sous deux états (*nommé* ON et OFF dans le code).

Code MicroPython :
```python
import pyb
import time
from pyb import Pin

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)


while True :
	time.sleep_ms(500)      # sleep for 500 milliseconds

	print(p_in.value()) # get value, 0 or 1

	if(p_in.value() == 1):
		print("ON")
	else:
		print("OFF")
```

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
