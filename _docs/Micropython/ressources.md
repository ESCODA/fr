---
title: Ressources
description: Ressources

---
# Resources

## Documentation

**Site officiel de MicroPython :** 

[https://micropython.org/](https://micropython.org/)

**Documentation général sur MicroPython :** 

[http://docs.micropython.org/en/latest/](http://docs.micropython.org/en/latest/)

**Exemple d’utilisation de la librairie pyb (très utile) :**

_Attention le pinout est différent de la cartepyB V1.1 :_

[http://docs.micropython.org/en/latest/pyboard/quickref.html#general-board-control](http://docs.micropython.org/en/latest/pyboard/quickref.html#general-board-control)

**Code source du projet MicroPython :** 

[https://github.com/micropython/micropython](https://github.com/micropython/micropython)

**Site officiel de Python 3 :**

[https://www.python.org/](https://www.python.org/)

**Documentation technique de la NUCLEO-WB55 :** 

[https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html)

**Documentation de l’application Android BLESTSensor :**

[https://github.com/STMicroelectronics/STBlueMS_Android](https://github.com/STMicroelectronics/STBlueMS_Android)


## Pinout du kit NUCLEO-WB55


Voici la correspondance des connecteurs avec le numéro des Pins du microcontrôleur:

![Tableau](images/pins.png)


## Description des Pin du STM32

![Tableau](images/tableau1.png)

![Tableau](images/tableau2.png)
