---
title: Exercices avancés en MicroPython avec la carte d'extension IKS01A3
description: Exercices avancés en MicroPython avec la carte d'extension IKS01A3

---
# Exercices avancés MicroPython avec la carte d'extension IKS01A3

Vous devrez disposez de la carte d’extension IKS01A3 pour ces exercices.

## Démarrage

La carte d’extension IKS01A3 est une carte de démonstration de plusieurs capteurs MEMS de ST Microelectronics. Sa version A3 contient les capteurs suivants: 

* [LSM6DSO](https://www.st.com/content/st_com/en/products/mems-and-sensors/inemo-inertial-modules/lsm6dso.html#tools-software) : Accéléromètre 3D + Gyroscope 3D
* [LIS2MDL](https://www.st.com/content/st_com/en/products/mems-and-sensors/e-compasses/lis2mdl.html) : Magnétomètre 3D
* [LIS2DW12](https://www.st.com/content/st_com/en/products/mems-and-sensors/accelerometers/lis2dw12.html) : Accéléromètre 3D
* [LPS22HH](https://www.st.com/content/st_com/en/products/mems-and-sensors/pressure-sensors/lps22hh.html) : Baromètre (260-1260 hPa)
* [HTS221](https://www.st.com/content/st_com/en/products/mems-and-sensors/humidity-sensors/hts221.html) : Capteur d’humidité relative 
* [STTS751](https://www.st.com/content/st_com/en/products/mems-and-sensors/temperature-sensors/stts751.html) : Capteur de température (–40 °C to +125 °C) 

Ces capteurs sont raccordés au bus *I2C* de la carte NUCLEO-WB55.

La carte d’extension *IKS01A3* dispose également d'un emplacement au format DIL 24 broches pour y ajouter des capteurs I2C supplémentaires (par exemple, le gyroscope [A3G4250D](https://www.st.com/en/evaluation-tools/steval-mki125v1.html)) 

![image](images/iks01a3.png)

## Démarrage

Brancher la carte *IKS01A3*, attention à bien respecter le marquage des connecteurs : CN9 -> CN9, CN5 -> CN5, etc.

Une fois que vous aurez raccordé correctement la carte d’extension, voici la liste des capteurs avec lesquels vous pouvez dialoguer par I2C.

## Utilisation de l'accéléromètre LIS2DW12

Pour cette démonstration, nous avons choisi d’utiliser l'accéléromètre 3 axes (LIS2DW12).  

La démonstration consiste à allumer une LED selon l’axe sur lequel l’accélération (-1g) due à la gravité est détectée. 

Voici la configuration axes de l’accélération / couleurs de LED : 

*   Axe x : LED verte
*   Axe y : LED bleu
*   Axe z : LED rouge

![image](images/iks01a3-3dir.png)


Comme pour chaque utilisation de librairie externe, copier le fichier ***LIS2DW12.py*** (disponible dans le [TP IKS](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)) dans le répertoire du disque USB : ***PYBFLASH***. 


Éditez maintenant le script ***main.py*** (ou telecharger directement le [TP IKS](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)) : 

``` python
from machine import I2C
import LIS2DW12
import pyb
import time

i2c = I2C(1) # On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur LISDW12
accelerometre = LIS2DW12.LIS2DW12(i2c)

led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while(1):
    time.sleep_ms(500)
    if abs(accelerometre.x()) > 700 : # Si la valeur absolue de l'accélération sur l'axe X est supérieur à 700 mG alors
        led_vert.on()
    else:
        led_vert.off() 
    if abs(accelerometre.y()) > 700 : # Si la valeur absolue de l'accélération sur l'axe Y est supérieur à 700 mG alors
        led_bleu.on()
    else:
        led_bleu.off() 
    if abs(accelerometre.z()) > 700 : # Si la valeur absolue de l'accélération sur l'axe Z est supérieur à 700 mG alors
        led_rouge.on()
    else:
        led_rouge.off() 
```

## Autres capteurs

Vous pourrez tout de même utiliser les autres capteurs de la carte d’extension grâce aux scripts disponibles dans le [TP IKS](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)

A noter, les 3 capteurs suivants permettent de construire les bases d'une station météo connectée.

*  STTS751 : Capteur de température (–40 °C to +125 °C) 
*  HTS221 : Capteur d’humidité relative 
*  LPS22HH : Baromètre (260-1260 hPa)
