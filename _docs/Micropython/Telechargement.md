---
title: Telechargement
description: Liens de telechargement des firmware et TP

---
# Téléchargement STM32microPython
**Firmware**<br>
[Firmware_1.13](https://drive.google.com/drive/folders/1hrllG6rvtkksMqYClUx0DB360AFfaxR5?usp=sharing)

**TP**<br>
[TP GPIO in](https://drive.google.com/drive/folders/1DiLyczHdsHKvI42AuZqsXHzCuAQto4HH?usp=sharing) (Lecture de l’état d’un bouton)<br>
[TP GPIO out](https://drive.google.com/drive/folders/1Yse62WDkIBrS6D6eZf7W5pKY6UysAN7v?usp=sharing) (Allumer une LED)<br>
[TP ADC](https://drive.google.com/drive/folders/1XAsawgi9TZguRd0xsCB6ym-X1msduJkA?usp=sharing) (Lecture d’une valeur analogique)<br>
[TP LCD](https://drive.google.com/drive/folders/1pZDNmAUu1uJ1fwTnsKhNiW2FjeUjvQjE?usp=sharing) (Affichage sur un écran LCD OLED)<br>
[TP BLE](https://drive.google.com/drive/folders/1fCdS52mXV_JdiAr9_qPNXRR4T7YEm5BE?usp=sharing) (Utilisation du BLE)<br>
[TP IKS](https://drive.google.com/drive/folders/1O4NCGGCs-lEP9cPNM-8MNryFrJnroSp5?usp=sharing) (Utilisation des capteurs de la carte IKS01A3: accéléromètre...)<br>

**TP Grove**<br>
[DEL Infrarouge](https://drive.google.com/drive/folders/1brZTXDqQQnRLv1i4Y-xkD1F-fRYB-cUC?usp=sharing) (Utilisation de la LED IR)<br>
[Potentiométre](https://drive.google.com/drive/folders/1SBU0z-jxGM6Tuw23r6XrN4WAArMSoHSn?usp=sharing) (Utilisation du potentiométre Grove)<br>
[Luminosité](https://drive.google.com/drive/folders/1itcd2G1O9j0yV1GqZISoculRMxS_oA71?usp=sharing) (Utilisation du capteur de luminosité)<br>
[Afficheur 7 segments](https://drive.google.com/drive/folders/1292BZYu139T8C7LOO9hkq90By1JZLoTH?usp=sharing) (Utilisation de l'afficheur 7 segments)<br>
[Alarme](https://drive.google.com/drive/folders/17-AiX-s7BcV0JSwIUTA1IoGFgAVjD05N?usp=sharing) (Detection de mouvement)<br>
[Buzzer](https://drive.google.com/drive/folders/1Djq0uu6mjPGXdY3nkpXBhYjpPsijHzwy?usp=sharing) (Utilisation du buzzer)<br>

**Package TP**<br>
[Totalité des TP](https://drive.google.com/drive/folders/1FfR_1aCuRDfYxSFcZ6ScvXtl9ZvoyK6S?usp=sharing) (Fichier zip comportant la totalité des scripts permettant d'executer les TP)<br>
