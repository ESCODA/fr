---
title: Guide de démarrage rapide Linux
description: Guide de démarrage rapide Linux

---
# Tutoriel d'utilisation de MicroPython depuis une machine virtuelle Linux

**Outils nécessaires à l'installation de MicroPython :**  
Il est nécessaire d'utiliser un __ordinateur Windows avec une machine virtuelle Linux__ installée ou un _ordinateur Linux directement_.

**Pour obtenir une machine virtuelle :**  
Vous trouverez une machine virtuelle « Polytech » en suivant [ce lien](https://polytech-prog.gricad-pages.univ-grenoble-alpes.fr/polytech-microc/).
Allez dans la catégorie « Ressources pour programmer le STM32 sur un ordinateur personnel »,
puis en cliquant sur « Avec VirtualBox ».


## Installation de MicroPython pour STM32 sur un ordinateur Linux  

Depuis le bureau Linux, ouvrez un terminal en faisant
Clic Droit puis « Ouvrir un terminal ici »  
Entrez ensuite les commandes suivantes une par une
afin d’installer les logiciels pré-requis.  

```bash
sudo apt-get install git
sudo apt-get install make
sudo apt-get install gcc
sudo apt-get install gcc-arm-none-eabi
```

Entrez votre mot de passe lorsque celui-ci vous est demandé.
Appuyez sur la touche « o » pour accepter l’installation lorsque cela vous sera demandé.  
Une fois les logiciels pré-requis installés, il est nécessaire de récupérer le projet MicroPython depuis
l’outil git en écrivant dans un terminal (ouvert depuis un dossier où sera placé l'utilitaire pour Micropython) les commandes suivantes :

```bash
git clone https://github.com/micropython/micropython
cd ~/micropython
git submodule update --init
cd mpy-cross
make
cd ../ports/stm32
```

Vous pouvez maintenant effectuer la commande suivante :

```bash
make BOARD={your-board-model}
```

Il est nécessaire de remplacer *{your-board-model}* par le nom de la carte STM32 utilisée.
Par exemple, si vous utilisez un *NUCLEO F446RE*, il sera nécessaire d’écrire la commande :  

```bash
make BOARD=NUCLEO_F446RE
```

Une liste des cartes STMicroelectronics compatibles avec MicroPython est disponible [ici](https://github.com/micropython/micropython/tree/master/ports/stm32/boards).

* Il est possible que la commande make ne fonctionne pas comme voulu, dans ce cas il faut ajouter

```bash
PYTHON=python2 pour obtenir la commande suivante :
make BOARD={your-board-model} PYTHON=python2
```

* Vous avez réussi cette étape si le terminal affiche :  

![make-board.png](images/make-board.png)

Les commandes précédentes ont permis de générer un dossier
nommé `build-{your– board–model}` disponible dans  
`/micropython/ports/stm32` (le dossier micropython se trouve là où vous avez ouvert le terminal)  

Ouvrez l’explorateur de fichier pour récupérer ce dossier. Il contient un fichier avec une extension `.hex`, il s’agit du moteur MicroPython que nous allons flasher dans le STM32.  

Pour cela, nous utiliserons STM32CubeProgrammer téléchargeable [ici](https://my.st.com/content/my_st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-programmers/stm32cubeprog.license=1575477973406.product=STM32CubeProg.version=2.2.1.html#get-software). Vous devrez vous créer un compte myST.  

- 1 - Installation sur un evironnement Linux :

Ouvrez un terminal et entrez les commandes suivantes :

```bash
sudo apt-get install libusb-1.0.0-dev    
sudo apt install openjdk-8-jre-headless
sudo apt purge openjfx  
sudo apt install openjfx=8u161-b12-1ubuntu2 libopenjfx-jni=8u161-b12-1ubuntu2 libopenjfx-java=8u161-b12-1ubuntu2  
sudo apt-mark hold openjfx libopenjfx-jni libopenjfx-java
sudo apt-get update
```  
Entrez ensuite :
```bash  
sudo update-alternatives --config java
```  
Si le terminal vous affiche `Rien à configurer`, passez à la suite, sinon plusieurs lignes vous seront affichées avec des numéros au début. Entrez le numéro de la ligne contenant **java-8-openjdk**, et passez à la suite.  

Extrayez l'archive téléchargée, puis exécutez **SetupSTM32CubeProgrammer-x.y.z.linux** (x,y,z varient selon la version)  
Laissez-vous guider par l'installeur.  

Entrez ensuite dans un terminal la commande :  
```bash
sudo cp ~/STMicroelectronics/STM32Cube/STM32CubeProgrammer/Drivers/rules/*.* /etc/udev/rules.d
```  
- 2 - Installation sur un environnement Windows :  

Copiez le fichier `.hex` sur clé USB pour ensuite passer sur un environnement Windows.  
Extrayez l'archive téléchargée, puis exécutez **SetupSTM32CubeProgrammer-x.y.z.exe** (x,y,z varient selon la version)  
Laissez-vous guider par l'installeur.  

`STM32CubeProgrammer` est maintenant installé.  

Ouvrez alors le fichier `.hex` (1) dans STM32CubeProgrammer et connectez la carte Nucleo à l'ordinateur.  
Sur le logiciel, cliquez sur `connect` (2) puis `download` (3) et enfin `verify` (3).  
Si vous rencontrez une erreur lors de la connexion, essayez de rebrancher le cable ou changez de port USB.   

![cube-programmer.png](images/cube-programmer.png)  

Le code est flashé sur la carte si on observe un message de succès de vérification.  
Vous avez alors réussi à installer MicroPython sur une carte STM32.  


## Premiers codes en MicroPython  

Ouvrez un terminal série comme gtkterm ou PuTTY.   
Choisissez le port série correspondant à votre carte STM32 (`gestionnaire de périphérique > ports(COM et LPT) > STMicroelectronics`, le port est entre parenthèses sur Windows et `/dev/ttyACM0` sur Linux), avec une vitesse de 115200.  
Vous devriez alors observer après avoir appuyé sur le bouton RESET noir un message similaire :

![reset-button.png](images/reset-button.png)

Il est alors possible d’utiliser le terminal comme un interpréteur Python,
il s’agit de l’interpréteur interactif MicroPython nommé REPL.
Entrez maintenant les lignes suivantes pour tester le langage Python :

```python
print(« Hello World »)
for i in range (10) :
```

Vous remarquerez que les inscriptions `>>>` sont devenues `...` après la seconde commande, cela est normal car la notion d’indentation est très importante dans le langage Python contrairement à d’autres langages comme le C par exemple.  
Entrez alors par la suite `print(i)` puis appuyez sur la touche Entrée trois fois vous observez alors :

![print-i.png](images/print-i.png)  

Nous avons alors créé une variable nommée `i` et nous l’avons incrémenté jusqu’à la valeur `9`.  
Entrez `help()` pour avoir quelques commandes propres au langage MicroPython.


## Utilisation du logiciel Geany pour lire un programme MicroPython  

Nous savons maintenant comment programmer en MicroPython sur un STM32 à partir d’un terminal série. Cela peut alors devenir contraignant dans le cas d’un programme de plusieurs dizaines de lignes car il faut alors écrire ligne après ligne en prenant compte de l’indentation du langage Python.  
Nous allons alors « automatiser » l’outil de développement Geany de manière à « compiler » un programme écrit en MicroPython.  

Dans un premier temps, il est nécessaire d’installer le logiciel Geany :  
* Sur un environnement Linux, entrez cette commande sur un terminal :

```bash
sudo apt-get install geany
```

* Sur un environnement Windows, télécharger le logiciel [ici](https://www.geany.org/download/releases/).  

Récupérez ensuite le fichier `pyboard.py` fournit avec le produit.  
Ce fichier contient un programme écrit en langage Python permettant d’envoyer les programmes MicroPython à la carte STM32 depuis un poste fixe.  
Créez un répertoire sur votre ordinateur et déposez le fichier pyboard.py dedans.  
Il faut alors configurer le logiciel Geany.
Après avoir ouvert le logiciel, allez dans le menu `Construire > Définir les commandes de construction` pour ajouter un nouveau bouton nommé `MicroPython` dans la partie `Commandes d'exécution`.  
Dans le champ suivant, écrivez :

```bash
python pyboard.py --device '/dev/ttyACM0' "%f"** pour Linux
```

```
python pyboard.py --device COMx "%f"
```
pour Windows, remplacez x par le numéro que vous pouvez trouver sous `Gestionnaire de périphérique > ports(COM et LPT) > STMicroelectronics`, le numéro est entre parenthèses.  
Attention : le port peut changer entre chaque carte et port USB.


Vous devez obtenir :  

![geany-compilateur.png](images/geany-compilateur.png)  

Vous pouvez à présent créer un fichier du nom de votre choix avec une extension `.py` (créez un document texte et ajoutez manuellement `.py`) et le placer dans le répertoire que vous venez de créer.  
Ouvrez-le avec Geany puis exécuter ce programme avec `Construire > MicroPython`.  

Ce tutoriel est terminé, vous pouvez écrire un script et l’exécuter depuis Geany !

## Travaux pratiques avec MicroPython  

Dans un premier temps nous allons voir comment allumer une LED.  
Pour cela, écrivez le code ci-dessous directement sur un émulateur série ou sur l’IDE Geany :  

```python
from pyb import LED
led = LED(1)
led.on()
```

Ce code permet d’allumer la LED utilisateur de votre microcontrôleur, cette LED est généralement
de couleur verte.

* Il est très important de commencer un programme MicroPython pour STM32 avec l’importation
de la bibliothèque pyb ou d’une de ces composantes (LED, Pin, ExtInt, Timer .Etc).
Ajouter en début (première ligne) de votre programme :

```python
import pyb
```

Introduction aux entrées/sorties :  
```python
from pyb import Pin

p_out = Pin('PA5', Pin.OUT_PP) # LED verte correspondant à LED(1)
p_out.high() # p_out.low() pour éteindre la LED

p_in = Pin('PC13', Pin.IN, Pin.PULL_UP)
p_in.value() # prend la valeur 0 ou 1
```

La LED utilisateur doit s’allumer ici aussi. La fonction `p_in.value()` doit retourner et afficher un bit prenant la valeur 0 ou 1 suivant l’état de `PC13` (généralement le bouton poussoir utilisateur).

Nous avons appris à allumer une pin en sortie et à lire la valeur d’une pin en entrée, nous allons à
présent voir comment générer une PWM.  
On utilise la modulation de largeur d’impulsion (MLI) ou Pulse Width Modulation (PWM) lorsque
l’on veut générer un signal pseudo-analogique depuis une source numérique.  
Les signaux PWM ont pour caractéristique :
* Une fréquence, qui désigne la vitesse à laquelle le signal a effectué un cycle complet
(10Hz → 10 cycles par seconde).
* Un rapport cyclique (Duty Cycle) qui correspond à la durée à l’état haut sur la période du signal

![duty-cycle-examples.png](images/duty-cycle-examples.png)  

Avant de générer une PWM, il est impératif de savoir si la broche du STM32 que nous voulons
utiliser est compatible avec cette modulation et également le numéro du timer (ainsi que son canal) utilisé pour la modulation.  
Par exemple, la pin PA5 généralement reliée à la LED utilisateur sur une majorité de carte Nucleo
peut être relié électriquement au Timer 2 canal 1 sur certains STM32F4.  

Dans ce cas, le code pour faire clignoter cette LED avec une PWM de fréquence 1Hz et avec un
rapport cyclique de 50% sera :

```python
from pyb import Pin, Timer
LED = Pin('PA5') # PA5 --> TIM2, CH1
tim = Timer(2, freq=1)
ch = tim.channel(1, Timer.PWM, pin=LED)
ch.pulse_width_percent(50) #DutyCycle de 50%
```

Le rapport cyclique est de 0,5 sur une fréquence de PWM de 1Hz, la LED est donc allumé durant
500 millisecondes et éteinte durant 500 millisecondes également.

Une liste de bouts de codes MicroPython est disponible [ici](http://docs.micropython.org/en/latest/pyboard/quickref.html#general-board-control)  

**Attention :**  
Ces codes ne sont pas encore tous compatibles avec les microcontrôleurs STM32,
ils sont destinés à la carte pyboard (une carte d’expérimentation programmable nativement en
MicroPython).  


## Références  
* [Flashing Micropython to STM32 Nucleo/](http://awhtechnologies.com/flashing-micropython-to-stm32-nucleo/)
* [MicroPython : Pour commencer](http://micropython.fr/installation/de_quoi_avez_vous_besoin)  
* [Interagir avec micropython depuis un script Python](http://micropython.fr/technique/python_to_micropython)
* [UM2237 User manual STM32CubeProgrammer software description](https://www.st.com/content/ccc/resource/technical/document/user_manual/group0/76/3e/bd/0d/cf/4d/45/25/DM00403500/files/DM00403500.pdf/jcr:content/translations/en.DM00403500.pdf)
* [Illustration PWM](https://en.wikipedia.org/wiki/File:Duty_Cycle_Examples.png)  
