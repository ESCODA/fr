---
title: Guide de démarrage rapide Windows 10
description: Guide de démarrage rapide Windows 10

---
# Guide de démarrage rapide Windows 10

> Ces étapes sont à réaliser par le professeur ayant un accès administrateur sous Windows 10, pour chaque carte NUCLEO WB55 utilisée en TP. 

Dans ce guide de démarrage, nous allons:
- Programmer la carte avec le micrologiciel
- Installer l'environement de programmation sur votre PC
- Utiliser un terminal de communication UART (PUTTY)
- Programmer sur la Nucleo notre premier script Python

## Configuration électronique

**Vous aurez besoin d’un câble USB vers micro USB.**

Cette configuration se résume à :

- Déplacer le cavalier sur USB_STL
- Connecter un câble micro USB sur le port ST_LINK en dessous de la LED rouge

Voici comment doit être configuré le kit :

![image](images/Nucleo_WB55_Config_Programmation.svg)


Ces modifications permettent de configurer le STM32WB55 en mode programmation STL_Link. Nous pourrons ainsi mettre à jour le firmware par USB_ST_Link.

## Programmation des cartes

Téléchargez sur le Bureau Windows le dernier [Firmware](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) et extraire le dossier **TP_Python** sur le bureau

Ce dossier devra être présent sur chaque poste de TP pour les étudiants.

Voici la liste des fichiers présents dans le dossier :

![Image](images/dossier_tp_python.png)

Reliez la carte NUCLEO-WB55(USB_USER) avec le câble USB à votre ordinateur.

La première étape consiste à installer les drivers USB libres pour communiquer avec la carte STM32WB55. Pour cela, nous utiliserons le logiciel Zadig.

### Installation du driver USB libre (Zadig)

Double-cliquez sur le logiciel ***Zadig***.

![Image](images/autorisation_zadig.png)

Autorisez l'application à apporter des modifications en cliquant sur le bouton ***Oui***.

Voici l'interface :

![Image](images/zadig_1.png)

Allez dans ***Options*** puis cliquer sur *List All Devices*.

![Image](images/zadig_2.png)

Sélectionnez ***USB DFU in FS mode*** grâce à la liste déroulante.<br>
Si cela n'apparaît pas dans la liste déroulante, changez de câble USB (certains câbles ne fonctionnent pas).

![Image](images/zadig_3.png)

Sélectionnez ***libusbK (v3.0.7.0)*** en utilisant les flèches :

![Image](images/zadig_4.png)

Lancez ensuite l'installation du driver en cliquant sur ***Replace Driver***.

![Image](images/zadig_5.png)

Nous utilisons désormais la communication USB avec la carte NUCLEO-WB55 sous Windows.

Quittez le logiciel Zadig.


### Programmation du firmware MicroPython de la carte (dfu-util)

Il faut maintenant programmer la carte avec le firmware MicroPython. Nous utiliserons pour cela le logiciel **dfu-util**. Cette étape pourrait potentiellement être effectuée par l'étudiant, dans le cas où il faudrait mettre à jour le firmware MicroPython.

Utilisez le fichier ***flash_dfu-util.bat*** en double-cliquant dessus.<br>

Puis, autorisez Windows à exécuter le programme.

Une fenêtre de commande Windows s'ouvre (il se peut que ce soit trop rapide pour le voir), veuillez attendre que la programmation se finisse :

![Image](images/dfu_util_1.png)

La programmation est maintenant terminée, la fenêtre se ferme.

Vous devez maintenant déplacer le cavalier du connecteur CN7, PIN 5-7 sur SWDIO :

![Image](images/Nucleo_WB55_Config_Programmation_2.png)

Appuyez sur le bouton ***Reset(SW4)*** au dessus de la prise ***USB_USER*** sur le kit de développement.

Un message Windows apparaît :

![Image](images/dfu_util_2.png)

Ne prenez pas en compte ce message, concernant les problèmes du lecteur.

Si vous voyez ce message, c'est que l'opération s'est bien passée.

Vous pouvez maintenant communiquer avec le logiciel MicroPython via la liaison USB.

Regardons les fichiers générés par le système MicroPython avec l'explorateur Windows :

Ouvrir avec l'explorateur Windows, le périphérique PYBFLASH.

![Image](images/dfu_util_3.png)

Nous verrons plus tard comment éditer les scripts Python disponibles dans le système de fichier PYBFLASH.

Tout d'abord nous allons essayer de communiquer avec l'interpréteur de commande Python directement sur le kit de développement NUCLEO-WB55.



## Installation de l'environnement de programmation

Trois solutions s'offrent a vous:
- Installer(droits admin requis) [Thonny](https://thonny.org/) et son terminal integré
- Installer(droits admin requis) [Notepad++](https://notepad-plus-plus.org/downloads/) avec le plugin de [coloration synthaxique Python](http://npppythonscript.sourceforge.net/download.shtml) et un terminal serie (PuTTY, [TeraTerm](https://ttssh2.osdn.jp/index.html.en)...)
- Utiliser la version portable de PyScripter(fournie dans le package) et un terminal serie (PuTTY, [TeraTerm](https://ttssh2.osdn.jp/index.html.en)...). Dans ce dernier cas, suivez le tutoriel suivant.

Ces manipulations devront être maîtrisées par les étudiants afin qu'ils puissent communiquer avec l'interpréteur et éditer des scripts MicroPython sur le kit NUCLEO-WB55.

## Configuration du terminal de communication

Maintenant que MicroPython est présent sur le kit NUCLEO-WB55, nous voudrions tester la communication avec Python. Le test consiste à envoyer une commande python et vérifier que l'exécution s'effectue.

La communication s'effectue par USB par le biais d'un port série, nous avons donc besoin d'un logiciel capable d'envoyer les commandes python sous forme de texte à la carte et de recevoir le résultat de l'exécution.

Pour cela, lancez l'utilisateur Puttytel, disponible dans le répertoire TP_Python :

![Image](images/putty_1.png)

Sélectionnez l'option `Serial` :

![Image](images/putty_2.png)

Configurez les champs `Serial Line` en `COM3` et `Speed` à `115200`. Puis cliquez sur `Open`.

Si le message suivant apparaît :

![Image](images/putty_3.png)


Vérifiez que le kit est bien énuméré sur le bon port COM, voir **Vérifier l'attribution du port COM**.

Sinon, une nouvelle fenêtre s'affiche :

![Image](images/putty_4.png)


Appuyez sur CTRL+C pour faire apparaître l'interpréteur de commande python :

![Image](images/putty_5.png)


Vous pouvez maintenant exécuter des commandes Python. 

```python
print("Hello World")
```

![Image](images/putty_6.png)

Nous avons maintenant terminé la programmation du firmware MicroPython.

Notre kit de développement NUCLEO-WB55 est désormais prêt à être utilisé avec Python.


Gardez la fenêtre Putty ouverte, elle vous sera utile pour voir l'exécution des scripts.

**NOTE :** L'autocomplétion (appui sur TAB) est disponible lors de la communcation série.


## Configuration de l'éditeur de script python

Nous allons voir, dans cette partie, comment mettre en place un environnement de développement MicroPython.


### Installation de l'environnement


Exécutez l'installateur `Portable Python-3.8.0 x64.exe`, l'installateur vous demande où extraire l'environnement Python. 
Utilisez le bouton `...` pour sélectionner le dossier `TP_Python` sur votre Bureau. 

![Image](images/pyscript_1.png)

Puis attendez la fin de l'extraction des fichiers.

Nous disposons maintenant d'un environnement de développement Python avec lequel le développeur pourra écrire des scripts MicroPython.

L'environnement est disponible dans le dossier `TP_Python\Portable Python-3.8.0 x64` :

![Image](images/pyscript_2.png)

Lancez l'outil `PyScript-Launcher` : 

![Image](images/pyscript_3.png)

Cet outil vous permettra de débugger et d'exécuter vos scripts python.

**Remarques importantes :**

> La libraire `pyb`, c'est-à-dire le groupement des fonctions micropython d'accès aux périphériques du microcontrôleur, n'est pas disponible lors du débogage des scripts sous Windows.

> Attention aussi à n'utiliser que les librairies Python-3 portées sous micropython. Par exemple la librairie `numpy` n'est pas encore implémentée officiellement dans MicroPython, il sera donc impossible d'effectuer un `import numpy` dans un script MicroPython à vocation d'être téléchargé dans la NUCLEO-WB55.

Voici la [liste des libraires utilisables ou en cours de développement sous MicroPython](http://docs.micropython.org/en/latest/library/index.html#micropython-specific-libraries)<br>

L'éditeur de script python (PyScript) est donc utile pour le test d'algorithme ou la complétion automatique des méthodes python.

### Ecrire et exécuter un script MicroPython

Revenez à l'éditeur de script PyScript-Launcher, puis développez le lecteur PYBFLASH :

![Image](images/pyscript_4.png)

Nous voyons ici 2 fichiers intéressants : `boot.py` et `main.py`.

Le script *boot.py*, à modifier par les utilisateurs avancés, permet d'initialiser MicroPython. Il permet notamment de choisir quel script sera exécuté après le démarrage de MicroPython, par défaut le script `main.py`.

Laissez la configuration initiale pour le moment.

Nous allons nous intéresser au script `main.py`.


Double-cliquez sur celui-ci :

![Image](images/pyscript_5.png)

Notre premier script va consister à afficher 10 fois le message `MicroPython est génial` avec le numéro du message et tout cela non pas sur notre Nucleo mais *****sous Windows*****.


Ecrivez l'algorithme suivant dans l'éditeur de script :

![Image](images/pyscript_6.png)


Exécutez et vérifiez le résultat de celui-ci en appuyant sur ![Image](images/pyscript_play.png)

![Image](images/pyscript_7.png)

Le script **fonctionne bien sous Windows**.

Nous voulons maintenant **l'exécuter sur la carte NUCLEO-WB55**.

Pour cela sauvegardez le script `main.py` (`CTRL+S`), puis revenez au terminal Putty. 

Le script sera exécuté lors d'un redémarrage de MicroPython.

Pour effectuer un redémarrage appuyez sur `CTRL+C` (pour afficher l'interpréteur `>>>`) puis CTRL+D (pour forcer le redémarrage) dans le terminal Putty : 

![Image](images/pyscript_8.png)

Notre script s'est exécuté avec succès ! Nous voyons bien notre message s'afficher 10 fois dans le terminal.

Nous sommes désormais prêt à effectuer des commandes MicroPython nous permettant d'accéder aux périphériques du microcontrôleur STM32WB55.

## Annexe : Vérifier l'attribution du port COM

Un port `COM` a dû être créé sur Windows.

Ecrivez `gestionnaire de périphériques` dans la bar de recherche Windows, puis cliquez sur `Ouvrir` :

![image](images/port_COM.png)

Une nouvelle fenêtre s'ouvre :

![image](images/gestion_peripheriques.png)

Relevez le numéro du port `COM`. Dans l'exemple ci-dessus, le kit NUCLEO est branché sur le port `COM3`. C'est ce numéro `COMx` qu'il faudra renter dans Putty.
