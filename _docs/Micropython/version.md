---
title: Versions de µPython et STM32WB
description: Versions de µPython et STM32WB

---

## Versions 

| Description | Auteur | Date | Révision | Firmware |
| ----------- | ------ | ---- | -- | --------------------- |
| Brouillon   | RNT 	| 20/12/2019 | Draft | | 	
| Première livraison pré-workshop | RNT | 14/01/2019 | 1 | [firmware_micropython](../../assets/Firmware/firmware_micropython.dfu) |
| Rajout du Bluetooth Low Energy | RNT | 19/02/2020 | 2 | [firmware_micropython](../../assets/Firmware/firmware_micropython.dfu) |
| Rajout de carte d’extension IKS01A3 | RNT | 30/03/2020 | 3 | [firmware_micropython](../../assets/Firmware/firmware_micropython.dfu) |

