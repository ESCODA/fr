---
title: Foire aux Questions
description: Foire aux Questions pour résoudre vos problèmes

---
# Foire aux Questions

<html>
    <ol type="1">
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Que faire quand une carte n'est subitement plus detectée par Windows ?</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Vérifier le câblage.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Essayer un autre port USB.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Changer de câble.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Re-flasher la carte, cf « Guide de démarrage Linux ou Windows10 ».</li>
            </ol>
        </li><br><br>
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Que faire quand Windows déclare les fichiers comme « corrupt file » ? Il n’est plus possible non plus de placer un fichier sur la carte, d’en réécrire un et même d’en éditer.</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Ceci arrive dans de rares cas lorsque la carte n’a pas été utilisée « proprement ». Voici quelques conseils :
                    <ol type="i">
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Utiliser Thonny ou Geany (téléchargeable depuis le net).</li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Vérifier que le bon port COM est détecté : <i>Tool</i> -> <i>Option</i> -> <i>Interpreter</i> puis se mettre sur <i>Micro Python</i> et <i>Port COM xx</i>.</li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Ouvrir le fichier depuis l'éditeur de texte. <i>File</i> -> <i>Open</i> puis sélectionner « <i>This Computer</i> » puis utiliser le navigateur pour aller chercher les fichiers sur la carte (PYBFLASH : ).</li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Utiliser Ctrl+D pour le soft reboot, n’utiliser le hard reset (bouton reset sur la carte) qu’en cas de non fonctionnement en soft.</li>
                    </ol>
                </li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Il faut re-flasher la carte.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Si ce n’est pas possible, formater la carte (décocher « <i>Quick Format</i> ») puis re-flasher la carte, après cette étape il est possible que Windows « ne voit pas » correctement le main.py ou le boot.py mais ce n’est en principe que de l’affichage de l’explorateur, ces fichiers sont présents sur la carte et les manips sont possibles.</li>
            </ol>
        </li><br><br>
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Que faire quand les capteurs de la carte d’extension IKS01-A3 ne renvoient que des « 0 » ?</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Tester les branchements.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Eviter de trop enfoncer les connecteurs.</li>
            </ol>
        </li><br><br>
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Que faire quand rien ne se passe avec le shield Grove ?</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Vérifier les branchements.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;S’assurer que l’interrupteur en bas à gauche du shield (en dessous du connecteur A0) est bien sur 3.3V.</li>
            </ol>
        </li><br><br>
    </ol>
</html>
