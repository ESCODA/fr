---
title: Kit STM32MicroPython
description: Description du kit pédagogique STM32MicroPython
categories: stm32 kit nucleo python
---

## Les cartes STM32

Les cartes STM32 Nucleo et Discovery de STMicroelectronics peuvent être programmées en C/C++ depuis
* l'environnement [Arduino IDE](https://github.com/stm32duino/wiki/wiki/Getting-Started)
* l'environnement [STM32Cube](https://www.st.com/stm32cube)
* l'environnement en ligne [MBed](https://os.mbed.com/platforms/ST-Nucleo-F446RE/)
* l'environnement [RIOT OS](https://github.com/RIOT-OS/RIOT/tree/master/boards/nucleo-f446re)

Les modèles F4xx peuvent être programmées en [microPython](https://micropython.org/stm32/).

Certains modèles de STM32 sont programmables via MicroPython.
Le kit pédagogique P-NUCLEO-WB55 en fait parti.

## Le Kit pédagogique P-NUCLEO-WB55

Le kit pédagogique STM32 Python comporte les cartes suivantes :

* [P-NUCLEO-WB55 Bluetooth™ 5 and 802.15.4 Nucleo Pack including USB dongle and Nucleo-68 with STM32WB55 MCUs](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html) (en vente chez [Farnell](https://fr.farnell.com/stmicroelectronics/p-nucleo-wb55/carte-iot-solut-mcu-arm-cortex/dp/2989052)).

* [X-NUCLEO-IKS01A3 - Carte d'extension, Capteur mouvement et environnent MEMS pour STM32 Nucleo, Arduino UNO Layout R3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html) (en vente chez [Farnell](https://fr.farnell.com/stmicroelectronics/x-nucleo-iks01a3/carte-d-extension-stm32-nucleo/dp/3106035)).

* [Carte fille](http://wiki.seeedstudio.com/Grove_System/#grove-starter-kit) de connection [Grove](http://wiki.seeedstudio.com/Grove_System/) et les capteurs fournis (potentiomètre, ultrasons, thermomètre, afficheur LED, récepteurs infra-rouge, afficheur LCD …) (en vente chez [Farnell](https://fr.farnell.com/seeed-studio/83-16991/grove-starter-kit-for-arduino/dp/2801858?st=Grove) et chez [Seeedstudio](https://www.seeedstudio.com/Base-Shield-V2.html)). _Cette carte est recommandée mais elle n'est pas obligatoire._


| | | |
|-|-|-|
![p-nucleo-wb55](images/p-nucleo-wb55.jpg) | ![x-nucleo-iks01a3](images/x-nucleo-iks01a3.jpg) | ![grove_starter_kit](images/grove_starter_kit.jpg)

Crédits des images :
* [STMicroelectronics](https://www.st.com)
* [Seeedstudio](http://wiki.seeedstudio.com/Grove_System) 

## Où acheter ce kit ?

En France, les cartes STM32 Nucleo du kit pédagogique SNT peuvent être achetées en ligne ou par bon de commande auprès des fournisseurs de composants électroniques comme [Farnell](https://fr.farnell.com), [RS Online](https://fr.rs-online.com/web/), [Conrad](https://www.conrad.fr) ou bien [Mouser](https://www.mouser.fr/).

En France, [Lextronic](https://www.gotronic.fr/), [GoTronic](https://www.gotronic.fr/) et [Conrad](https://www.conrad.fr) distribuent la plupart des platines du système [Grove de Seeedstudio](http://wiki.seeedstudio.com/Grove_System) et les platines I2C [Qwiic de Sparkfun](https://www.sparkfun.com/qwiic). _Pensez à commander suffisament de câbles de raccordement_.

## Autres kits

Remarque : d'autres kits sont envisageables pour des tutoriels STM32MicroPython. [Voir la liste ...](Kits)
