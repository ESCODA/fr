---
title: Comment utiliser Fritzing pour décrire des montages pour le kit STM32 MicroPython ?
description: Comment utiliser Fritzing pour décrire des montages pour le kit STM32 MicroPython ?
---
# Comment utiliser Fritzing pour décrire des montages pour le kit STM32 MicroPython ?

## Fritzing
[Fritzing](https://fritzing.org/) est une application qui permet de décrire des montages électroniques de manière pédagogique. Une importante bibliothèque de composants et des cartes (aka _parts_) est disponible.

## Sommaire
Vous trouverez dans cette partie un tutoriel sur l’utilisation de [Fritzing](https://fritzing.org/) avec le kit STM32 MicroPython.

## Installation de Fritzing
A rédiger

## Installation de la bibliothèque du _part_ STM32 Nucleo WB55 dans Fritzing
A rédiger

![Top](images/stm32nucleowb55-top.svg)
![Bottom](images/stm32nucleowb55-bottom.svg)

## Dessin d'un montage simple avec le composant STM32 Nucleo WB55
A rédiger

Installez les _parts_ Grove à partir du [dépôt Github](https://github.com/Seeed-Studio/fritzing_parts).


## Références
* [https://gitlab.com/stm32python/frizting](https://gitlab.com/stm32python/frizting)
