---
layout: page
title: STM32Python
permalink: /
---
# Bienvenue sur le site STM32MicroPython

<p align="center">
<img src="assets/logos/stm32upython.svg" width="400px" alt="logo"/>
</p>


## Contexte

La réforme des lycées introduit un nouvel enseignement suivi par tous les élèves de seconde générale et technologique :
[SNT (Sciences Numériques et Technologie)](https://eduscol.education.fr/cid143713/snt-bac-2021.html).
Un des thèmes abordés par cet enseignement est l’[Internet des objets (IdO, en anglais Internet of things, IoT)](https://fr.wikipedia.org/wiki/Internet_des_objets),
couvert par le chapitre "Informatique embarquée et objets connectés",
qui représente l’extension d’Internet à des choses et à des lieux du monde physique.

L’objectif est d’amener ces jeunes à un premier niveau de compréhension de l’internet des objets.
L’enjeu est de favoriser une orientation choisie, en l’occurrence ici vers l’ingénierie du numérique.
La part du « numérique » et de « l’informatique » dans les enseignements a été fortement augmentée avec la réforme du lycée.

## Objectif

L'objectif de STM32Python est de fournir aux enseignants du lycée et aux lycéens des supports pédagogiques open-source pour l'initiation
à l’Internet des Objets pour l’enseignement de
[SNT (Sciences Numériques et Technologie)](https://eduscol.education.fr/cid143713/snt-bac-2021.html).
Ces supports s'appuient sur la plateforme Nucleo STM32 de ST Microelectronics.
Ils permettent de réaliser des montages électroniques et des programmes pour les microcontroleurs STM32 avec les langages C/C++ et microPython.

Les supports réalisés sont également utilisables par d’autres enseignements de première et terminale générales, notamment en spécialité NSI (Numérique et Sciences Informatiques), en spécialité SI (Sciences de l’ingénieur), ou en série technologique STI2D (Sciences et Technologies de l’Industrie et du Développement Durable).

## Partenaires
Les partenaires de STM32Python sont:
* les rectorats des académies de [Grenoble](http://www.ac-grenoble.fr) et d’[Aix-Marseille](http://www.ac-aix-marseille.fr),
* [STMicroelectronics](https://www.st.com),
* [Inventhys](http://www.inventhys.com),
* [Polytech Grenoble](https://www.polytech-grenoble.fr), [Grenoble INP Institut d'ingénierie et de management](https://www.grenoble-inp.fr/), [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr).
