# Pages de documentation de STM32Python

## Getting Started
* [Install Jekyll](https://jekyllrb.com/docs/installation/)

### Start locally
```bash
git clone git@gitlab.com:stm32python/fr.git
bundle update
bundle install
bundle exec jekyll build
bundle exec jekyll serve
```

Browse http://127.0.0.1:4000/fr/

### Deploy

Push your repository and changes to GitLab. Then check the CI pipeline https://gitlab.com/stm32python/fr/-/pipelines

## References
* [Contributeurs](CONTRIBUTORS.md)
* [TODO List](TODO.md)
* [CHANGELOG](CHANGELOG.md)
* [LICENSE](LICENSE.md)

## Troubleshooting

[ci]: https://about.gitlab.com/gitlab-ci/
[Jekyll]: http://jekyllrb.com/
[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
